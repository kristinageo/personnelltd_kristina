<?php

namespace App\Http\Controllers;

use App\Models\CallInfo;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    //
    public function showUser($id)
    {

        $user = User::find($id);
        // dd($user);
        $score= $this->avgCallScore($user->id);
        $transactions= $this->last5Transactions($user->id);
        return view('display.user',['user' => $user,'score' => $score,'transactions' => $transactions]);
    }
    public function avgCallScore($id){
        //$user za koj user treba da barame vo callinfo tabelata
        $specific_user = CallInfo::where([['user_id',$id],['duration','>',10]])->get();
        $avgCallScore = $specific_user->avg('external_call_score');
        return $avgCallScore;
    }
    public function last5Transactions($id){
        // A table showing the last 5 call interactions for the user (for only valid calls*)
        $transactions = CallInfo::where([['user_id',$id],['duration','>',10]])->orderBy('date', 'DESC')->offset(0)->limit(5)->get();
        return $transactions;
    }
    public function showUsers(){
        // Valid calls are considered valid if the duration is greater than 10
        $users = User::all();
        return view('display.users',['users' => $users]);
     }
}
