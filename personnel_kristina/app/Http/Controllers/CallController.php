<?php

namespace App\Http\Controllers;

use App\Models\CallInfo;
use App\Models\Client;
use App\Models\Type_Call;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class CallController extends Controller
{
    //
    public function create(){
        $users = User::all();
        $clients = Client::all();
        $type_calls=Type_Call::all();
        return view('calls.create',['users' => $users,'clients' => $clients,'type_calls' => $type_calls]);
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user_id'=>'required',
            'client_id'=>'required',
            'type_call_id' => 'required',
            'date' => 'required',
            'duration' => 'required',
            'external_call_score' => 'required'
        ]);
        //user
        $user_id = User::where('user',$validatedData['user_id'])->firstOrFail()->id;//31
        //client
        $client_id = Client::where('client',$validatedData['client_id'])->firstOrFail()->id;//31
        //type_call
        $type_call_id = Type_Call::where('type_call',$validatedData['type_call_id'])->firstOrFail()->id;//31
        //date('d-m-Y', strtotime($user->from_date));
        //01/05/2022 1:30 PM
        $date = date('Y-m-d H:i:s', strtotime($validatedData['date']));//sega e vo ovoj format 2022-01-05 13:30:00
        $callinfo = CallInfo::create([
            'user_id'=> $user_id,
            'client_id'=>$client_id,
            'type_call_id' => $type_call_id,
            'date' => $date,
            'duration' => $validatedData['duration'],
            'external_call_score' => $validatedData['external_call_score']
         ]);
         if ($callinfo->exists) {
            return redirect()->route('calls.create')->with('success', 'Call created');
         } else {
            return redirect()->route('calls.create')->with('alert', 'Call not created');
         }
        

       
    }
    public function show($id)
    {
    
        $call = DB::table('callinfo')
        ->join('users', 'callinfo.user_id', '=', 'users.id')
        ->join('clients', 'callinfo.client_id', '=', 'clients.id')
        ->leftJoin('type_calls', 'callinfo.type_call_id', '=', 'type_calls.id')
        ->where('callinfo.id','=',(int)$id)
        ->select('callinfo.*', 'users.user', 'clients.client','type_calls.type_call')->get();
         return view('calls.show',['call' => $call]);
    }
    public function edit($id)
    {
    
         $users = User::all();
         $clients = Client::all();
         $type_calls=Type_Call::all();
         $callinfo = CallInfo::find( $id);
         // dd($callinfo);
         return view('calls.edit',['users' => $users,'clients' => $clients,'type_calls' => $type_calls,'callinfo' => $callinfo]);

    }
    public function update(Request $request,$id)
    {
 
   
         $user_id = $request->input('user_id');
         $client_id = $request->input('client_id');
         $type_call_id = $request->input('type_call_id');
         $date = $request->input('date');
         $duration = $request->input('duration');
         $external_call_score = $request->input('external_call_score');
         //  dd($duration);
         $user_id = User::where('user',$user_id)->firstOrFail()->id;//31
         //client
         $client_id = Client::where('client',$client_id)->firstOrFail()->id;//31
         //type_call
         $type_call_id = Type_Call::where('type_call',$type_call_id)->firstOrFail()->id;//31
         //  dd($type_call_id);
         //  //date('d-m-Y', strtotime($user->from_date));
         //  //01/05/2022 1:30 PM
         $date = date('Y-m-d H:i:s', strtotime($date));//sega e vo ovoj format 2022-01-05 13:30:00
         //   DB::update('update student set name = ? where id = ?',[$name,$id]);
         $callinfo = CallInfo::where('id', $id)->update([
            'user_id'=> $user_id,
            'client_id'=>$client_id,
            'type_call_id' => $type_call_id,
            'date' => $date,
            'duration' => $duration,
            'external_call_score' => $external_call_score
         ]);
         //  dd($callinfo);
         //  $callinfo->update($request->all());
         if($callinfo){
            return redirect()->route('displayCalls')
            ->with('success','Call updated successfully');
         }
         else{
            return redirect()->route('displayCalls')
            ->with('alert','Call not updated successfully');
         }
       
    }
    public function destroy($id)
    {

            $callinfo=CallInfo::find((int)$id);
            $callinfo->delete();
            return redirect()->route('displayCalls')->with('success', 'Call deleted');

    }
    public function displayValid(){
       // Valid calls are considered valid if the duration is greater than 10
       //callinfo da zememe duration
             $validcalls = DB::table('callinfo')
            ->join('users', 'callinfo.user_id', '=', 'users.id')
            ->leftJoin('clients', 'callinfo.client_id', '=', 'clients.id')
            ->leftJoin('type_calls', 'callinfo.type_call_id', '=', 'type_calls.id')
            ->leftJoin('type_clients', 'type_clients.id', '=', 'clients.type_client_id')
            ->where('duration','>',10)
            ->select('callinfo.*', 'users.user', 'clients.client','type_calls.type_call','type_clients.type_client')
            ->paginate(10);
             return view('display.valid',['validcalls'=>$validcalls]);
    }
    public function displayCalls(){
        // Valid calls are considered valid if the duration is greater than 10
            $calls = DB::table('callinfo')
            ->join('users', 'callinfo.user_id', '=', 'users.id')
            ->leftJoin('clients', 'callinfo.client_id', '=', 'clients.id')
            ->leftJoin('type_calls', 'callinfo.type_call_id', '=', 'type_calls.id')
            ->leftJoin('type_clients', 'type_clients.id', '=', 'clients.type_client_id')
            ->select('callinfo.*','users.user','clients.client','type_calls.type_call','type_clients.type_client')
            ->paginate(10);
            return view('display.calls',['calls'=>$calls]);
     }

}
