<?php

namespace App\Http\Controllers;

use App\Models\CallInfo;
use App\Models\Client;
use App\Models\Type_Call;
use App\Models\Type_Client;
use Illuminate\Http\Request;
use App\Models\User;

class CSVUpload extends Controller
{
    //
        public function index(){
            return view('welcome');
        }
        function csvToArray($filename = '', $delimiter = ',')
        {
            $file = 'files/'.$filename;
            // dd(file_exists($file));
            if (!file_exists($file) || !is_readable($file))
                return false;

            $header = null;
            $data = array();
            if (($handle = fopen($file, 'r')) !== false)
            {
                while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
                {
                    if (!$header)
                        $header = $row;
                    else
                        $data[] = array_combine($header, $row);
                }
                fclose($handle);
            }
            // dd($data);
            return $data;
        }
       public function CSVUpload(Request $req)
        {
                $req->validate([
                'file' => 'required|mimes:csv'
                ]);
                $file = $req->file('file');
                $name = time() . '-' . $file->getClientOriginalName();
                $destinationPath = 'files';
                $file->move($destinationPath,$name);
                $calls = $this->csvToArray($name);
                for ($i = 0; $i < count($calls); $i++)
                {
                 
                    $user = User::updateOrCreate([
                      'user' => $calls[$i]['User']
                    ]);
                    $type_client = Type_Client::updateOrCreate([
                      'type_client'=> $calls[$i]['Client Type']
                    ]);
                    $client = Client::updateOrCreate([
                      'client' => $calls[$i]['Client'],
                      'type_client_id' => Type_Client::where('type_client',$calls[$i]['Client Type'])->firstOrFail()->id
                    ]);
                    $type_call = Type_Call::updateOrCreate([
                        'type_call' => $calls[$i]['Type Of Call']
                    ]);
                    //   //user_id	client_id	type_call_id	date	duration	external_call_score
                    $callinfo = CallInfo::updateOrCreate([
                            'user_id' => User::where('user',$calls[$i]['User'])->firstOrFail()->id,
                            'client_id' => Client::where('client',$calls[$i]['Client'])->firstOrFail()->id,
                            'type_call_id' =>  Type_Call::where('type_call',$calls[$i]['Type Of Call'])->firstOrFail()->id,
                            'date' => $calls[$i]['Date'],
                            'duration' => (int) $calls[$i]['Duration'],
                            'external_call_score'=>(int) $calls[$i]['External Call Score']

                    ]);
                }
                //array:7 [▼
                //   "User" => "Daniel Graham"
                //   "Client" => "Phoebe King"
                //   "Client Type" => "Carer"
                //   "Date" => "2021-11-22 08:48:13"
                //   "Duration" => "427"
                //   "Type Of Call" => "Outgoing"
                //   "External Call Score" => "66"
                // ]
                return view('show',['successMsg'=>'File is uploaded into db.']);
            
               
        }

}
