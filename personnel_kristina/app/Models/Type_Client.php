<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type_Client extends Model
{
    use HasFactory;
    protected $fillable = [
        'type_client'
    ];
    protected $table = 'type_clients';
    public function type_clients(){
        return $this->hasMany(Client::class);
    }

}
