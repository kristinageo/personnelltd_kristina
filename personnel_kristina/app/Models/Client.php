<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;
    protected $fillable = [
        'client','type_client_id' 
    ];
    protected $table = 'clients';
    public function type_client()
    {
        return $this->belongsTo(Type_Client::class);
    }
    public function calls() {
        return $this->hasMany(CallInfo::class)->with('user', 'type_call');
    }
 

}
