<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class CallInfo extends Model
{
    use HasFactory;
    protected $fillable=['user_id','client_id','type_call_id','date','duration','external_call_score'];
    protected $table = 'callinfo';
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function client() {
        return $this->belongsTo(Client::class);
    }

    public function type_call() {
        return $this->belongsTo(Type_Call::class);
    }
}
