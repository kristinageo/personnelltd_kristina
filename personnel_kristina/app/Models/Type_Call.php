<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type_Call extends Model
{
    use HasFactory;
    protected $fillable = [
        'type_call'  
    ];
    protected $table = 'type_calls';
    public function calls() {
        return $this->hasMany(CallInfo::class)->with('client', 'user');
    }


}
