<?php

use App\Http\Controllers\CallController;
use App\Http\Controllers\CSVUpload;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', [CSVUpload::class, 'index'])->name('home');
Route::post('/csvUpload', [CSVUpload::class, 'csvUpload'])->name('csvUpload');
Route::get('/valid-calls', [CallController::class, 'displayValid'])->name('displayValid');
Route::get('/display-calls', [CallController::class, 'displayCalls'])->name('displayCalls');
Route::get('/show/{id}', [UserController::class, 'showUser'])->name('show.user');
Route::get('/display-users', [UserController::class, 'showUsers'])->name('showUsers');
Route::resource('calls', CallController::class);